<Qucs Schematic 0.0.20>
<Properties>
  <View=0,0,1119,1807,1,0,0>
  <Grid=10,10,1>
  <DataSet=noise_sim.dat>
  <DataDisplay=noise_sim.dpl>
  <OpenDisplay=1>
  <Script=noise_sim.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titel>
  <FrameText1=Erstellt von:>
  <FrameText2=Datum:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND *1 5 740 510 0 0 0 0>
  <R R1 1 740 480 15 -26 0 1 "1.3 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <C C1 1 610 370 -26 17 0 0 "50 uF" 1 "" 0 "neutral" 0>
  <R R2 1 610 270 -26 15 0 0 "10 GOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 5 420 490 0 0 0 0>
  <Vnoise V1 1 420 460 19 -26 0 1 "1" 1 "0" 0 "1" 0 "0" 0>
  <.AC AC1 1 180 1580 0 47 0 0 "log" 1 "0.01 Hz" 1 "100 Hz" 1 "100" 1 "yes" 0>
</Components>
<Wires>
  <420 270 580 270 "" 0 0 0 "">
  <420 270 420 370 "" 0 0 0 "">
  <420 370 420 430 "" 0 0 0 "">
  <420 370 580 370 "" 0 0 0 "">
  <640 270 740 270 "" 0 0 0 "">
  <740 270 740 370 "" 0 0 0 "">
  <640 370 740 370 "" 0 0 0 "">
  <740 370 740 450 "" 0 0 0 "">
  <740 370 950 370 "out" 880 340 113 "">
  <950 370 950 400 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
