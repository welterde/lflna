# Low Frequency Low Noise Amplifier


[![Schematic](releases/1.0.3/dc_lna.svg "Schematic")](releases/1.0.1/dc_lna.pdf)

![3D PCB View](releases/1.0.3/pcb_3d.png "3D PCB View")

![Component Placement](releases/1.0.3/pcb.png "Component Placement")
