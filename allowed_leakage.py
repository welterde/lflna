import numpy as np
import matplotlib.pyplot as plt


#dc_in = np.linspace(2.5, 20)

r_cap = np.logspace(np.log10(1e6), np.log10(10e9))
#print(r_cap)
r_lower = 20e3
#r_lower = 200e3

gain = 1e3

#input_voltage = 20
for input_voltage in [2.5, 5, 7, 10, 20]:
#for input_voltage in [0.25, 1, 2.5]:
    #gain = 500
    
    total_r = r_cap + r_lower
    current = input_voltage / total_r
    
    #print(current)
    
    opamp_v = r_lower * current
    
    output_v = gain*opamp_v
    #print(output_v)
    
    plt.plot(r_cap/1e6, output_v, label='Input Voltage=%f' % input_voltage)

plt.axvline(142, label='Selected Cap')

plt.legend()
plt.xscale('log')
plt.ylim(0, 3)
plt.grid(which='both', axis='both')
plt.xlabel('Capacitor Resistance [M$\Omega$]')
plt.show()
