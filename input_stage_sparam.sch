<Qucs Schematic 0.0.20>
<Properties>
  <View=0,-180,1391,1059,1,0,0>
  <Grid=10,10,1>
  <DataSet=input_stage_sparam.dat>
  <DataDisplay=input_stage_sparam.dpl>
  <OpenDisplay=1>
  <Script=input_stage_sparam.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Titel>
  <FrameText1=Erstellt von:>
  <FrameText2=Datum:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Pac P1 1 280 390 18 -26 0 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 280 530 0 0 0 0>
  <GND * 5 600 440 0 0 0 0>
  <Pac P2 1 810 360 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 5 810 390 0 0 0 0>
  <.DC DC1 1 180 780 0 47 0 0 "26.85" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "no" 0 "150" 0 "no" 0 "none" 0 "CroutLU" 0>
  <R R2 1 600 410 15 -26 0 1 "1.3 kOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <Eqn Eqn1 1 1190 760 -51 19 0 0 "gain=dB(S[1,2])+80" 1 "yes" 0>
  <Vdc V1 1 280 470 18 -26 0 1 "0 V" 1>
  <C C1 1 470 300 -26 17 0 0 "50 uF" 1 "" 0 "neutral" 0>
  <R R1 1 470 200 -26 15 0 0 "10 GOhm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <GND * 5 710 200 0 0 0 2>
  <.SP SP1 1 630 800 0 79 0 0 "log" 1 "0.01 Hz" 1 "100 Hz" 1 "100" 1 "yes" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Inoise I1 1 710 230 19 -26 0 1 "1" 1 "0" 0 "1" 0 "0" 0>
</Components>
<Wires>
  <280 500 280 530 "" 0 0 0 "">
  <280 420 280 440 "" 0 0 0 "">
  <280 200 280 300 "" 0 0 0 "">
  <280 200 440 200 "" 0 0 0 "">
  <280 300 280 360 "" 0 0 0 "">
  <280 300 440 300 "" 0 0 0 "">
  <500 200 600 200 "" 0 0 0 "">
  <600 200 600 300 "" 0 0 0 "">
  <500 300 600 300 "" 0 0 0 "">
  <600 300 600 380 "" 0 0 0 "">
  <600 300 710 300 "" 0 0 0 "">
  <810 300 810 330 "" 0 0 0 "">
  <710 300 810 300 "" 0 0 0 "">
  <710 260 710 300 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
