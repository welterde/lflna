import numpy as np
import matplotlib.pyplot as plt



current_noise = 100e-15 # 10 fA/\sqrt{Hz}

plt.title('$i_n = %.0f fA/\\sqrt{Hz}$' % (current_noise*1e15))

freq = np.logspace(-2, 2)

for c in [1e-3, 100e-6, 10e-6]:
    impedance = 1/2/np.pi/freq/c

    v_n = current_noise * impedance
    plt.loglog(freq, v_n, label='C=%.0fuF' % (c*1e6))

plt.grid()
plt.legend()
plt.show()

