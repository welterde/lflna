import numpy as np
import matplotlib.pyplot as plt



cap_size = np.logspace(-9, -2)

noise_voltage = np.sqrt(1.38e-23*300/cap_size)

current_noise = 10e-12 # 1pA/Hz

# impedance: 1/2/pi/f/C

freq_limit=0.05
current_noise_op = current_noise * 1/2/np.pi/freq_limit/cap_size

plt.loglog(cap_size*1e6, noise_voltage*1e9, label='Capacitor Noise')
plt.loglog(cap_size*1e6, current_noise_op*1e6, label='Opamp Current Noise')

plt.legend()
plt.grid()

plt.xlabel('Cap Size [uF]')
plt.ylabel('Noise Voltage [nV]')
plt.show()
